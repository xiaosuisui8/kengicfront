/** When your routing table is too long, you can split it into small modules **/
import Layout from '@/layout'

const tableRouter = {
  path: '/table',
  component: Layout,
  redirect: '/table/complex-table',
  name: 'Table',
  meta: {
    title: '基础数据',
    icon: 'table'
  },
  children: [
/*    {
      path: 'dynamic-table',
      component: () => import('@/views/table/dynamic-table/index'),
      name: 'DynamicTable',
      meta: { title: 'Dynamic Table' }
    },*/
/*    {
      path: 'drag-table',
      component: () => import('@/views/table/drag-table'),
      name: 'DragTable',
      meta: { title: 'Drag Table' }
    },*/
    {
      path: 'complex-table',
      component: () => import('@/views/table/complex-table'),
      name: 'ComplexTable',
      meta: { title: '产品管理' }
    },
    {
   		path: 'material-table',
      component: () => import('@/views/table/material-table'),
      name: 'MaterialTable',
      meta: { title: '物料管理' }
    },
    {
   		path: 'product-material',
      component: () => import('@/views/table/product_material'),
      name: 'ProductMaterialTable',
      meta: { title: '产品物料' }
    },
    {
   		path: 'dcscache',
      component: () => import('@/views/table/dcscache-table'),
      name: 'DcsCacheTable',
      meta: { title: 'A线缓存' }
    },
    {
   		path: 'dcscacheB',
      component: () => import('@/views/table/dcscacheb-table'),
      name: 'DcsCacheTable',
      meta: { title: 'B线缓存' }
    }
    
  ]
}
export default tableRouter
