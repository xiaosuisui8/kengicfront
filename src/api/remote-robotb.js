import request from '@/utils/request'

export function robotTaskList(query) {
  return request({
    url: '/getRobotTaskPageb',
    method: 'get',
    params: query
  })
}
export function deleteRobotTask(id) {
  return request({
    url: '/deleteRobotTaskb',
    method: 'get',
    params: id
  })
}
export function reSendCurrentRobot(query) {
  return request({
    url: '/reSendCurrentRobotb',
    method: 'get',
    params: query
  })
}

export function reSendNextRobot(query) {
  return request({
    url: '/reSendNextRobotb',
    method: 'get',
    params: query
  })
}

