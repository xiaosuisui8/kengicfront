import request from '@/utils/request'

export function searchUser(name) {
  return request({
    url: '/vue-element-admin/search/user',
    method: 'get',
    params: { name }
  })
}

export function transactionList(query) {
  return request({
    url: '/getStackingListPageb',
    method: 'get',
    params: query
  })
}
export function deleteStackingTask(id) {
  return request({
    url: '/deleteStackingTaskb',
    method: 'get',
    params: id
  })
}

export function reSendCurrentStacking(query) {
  return request({
    url: '/reSendCurrentStackingb',
    method: 'get',
    params: query
  })
}

export function reSendNextStacking(query) {
  return request({
    url: '/sendNextStackingb',
    method: 'get',
    params: query
  })
}

