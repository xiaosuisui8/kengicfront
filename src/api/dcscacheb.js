import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/getDcsCacheForPageB',
    method: 'get',
    params: query
  })
}


export function updateArticle(data) {
  return request({
    url: '/updateDcsCacheForPageB',
    method: 'post',
    data
  })
}

export function returnMaterialApi(query){
	  return request({
    url: '/goBackMaterialB',
    method: 'get',
    params: query
  })
}
export function callMaterialApi(query){
	  return request({
    url: '/callMaterialB',
    method: 'get',
    params: query
  })
}