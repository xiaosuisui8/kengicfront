import request from '@/utils/request'

export function robotTaskList(query) {
  return request({
    url: '/getRobotTaskPage',
    method: 'get',
    params: query
  })
}
export function deleteRobotTask(id) {
  return request({
    url: '/deleteRobotTask',
    method: 'get',
    params: id
  })
}
export function reSendCurrentRobot(query) {
  return request({
    url: '/reSendCurrentRobot',
    method: 'get',
    params: query
  })
}

export function reSendNextRobot(query) {
  return request({
    url: '/reSendNextRobot',
    method: 'get',
    params: query
  })
}

