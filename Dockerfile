FROM centos:7.8.2003
MAINTAINER xiaosuisuis
RUN yum install -y  gcc gcc-c++ automake pcre pcre-devel zlib zlib-devel openssl openssl-devel iproute net-tools iotop     
WORKDIR /opt    
RUN yum install wget -y
RUN wget http://nginx.org/download/nginx-1.8.0.tar.gz && \
    tar -xzvf nginx-1.8.0.tar.gz && \
    cd /opt/nginx-1.8.0 && \
    ./configure --prefix=/usr/local/nginx/ \
    --with-http_ssl_module \
    --with-http_stub_status_module && \ 
    make && make install
    WORKDIR /usr/local/nginx
    COPY dist ./html/
    COPY nginx.conf ./conf/
    ENV PATH $PATH:/usr/local/nginx/sbin
    EXPOSE 100
    CMD ["nginx", "-g", "daemon off;"]

